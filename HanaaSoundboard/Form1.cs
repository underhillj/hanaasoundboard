﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace HanaaSoundboard
{
    public partial class Form1 : Form
    {
        private SoundPlayer music;
        private Stream file;
        private IConnection connection;
        private IModel publishChanel;
        private IModel consumeChanel;

        public Form1()
        {
            var assembly = Assembly.GetExecutingAssembly();
            file = assembly.GetManifestResourceStream("HanaaSoundboard.letsgetiton.wav");

            var factory = new ConnectionFactory() { HostName = "10.1.21.64" };
            connection = factory.CreateConnection();

            publishChanel = connection.CreateModel();
            consumeChanel = connection.CreateModel();

            publishChanel.ExchangeDeclare("soundboard", ExchangeType.Fanout, false, true, null);

            consumeChanel.ExchangeDeclare("soundboard", ExchangeType.Fanout, false, true, null);
            consumeChanel.QueueDeclare(System.Net.Dns.GetHostName(), false, false, true, null);
            consumeChanel.QueueBind(System.Net.Dns.GetHostName(), "soundboard", "");

            InitializeComponent();
            music = new SoundPlayer(file);
            music.Load();

            var consumer = new EventingBasicConsumer(consumeChanel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);
                if (message == "play")
                {
                    music.Play();
                }
                else if (message == "stop")
                {
                    music.Stop();
                }
            };
            consumeChanel.BasicConsume(System.Net.Dns.GetHostName(), true, consumer);
        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void button1_MouseDown(object sender, MouseEventArgs e)
        {
            string message = "play";
            var body = Encoding.UTF8.GetBytes(message);

            publishChanel.BasicPublish(exchange: "soundboard",
                                 routingKey: "",
                                 basicProperties: null,
                                 body: body);
        }

        private void button1_MouseUp(object sender, MouseEventArgs e)
        {
            string message = "stop";
            var body = Encoding.UTF8.GetBytes(message);

            publishChanel.BasicPublish(exchange: "soundboard",
                                 routingKey: "",
                                 basicProperties: null,
                                 body: body);
        }
    }
}
